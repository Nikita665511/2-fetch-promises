
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
const usersJsonUrl = "https://jsonplaceholder.typicode.com/users";
const todosJsonUrl = "https://jsonplaceholder.typicode.com/todos";

function fetchJson(url) {
    return new Promise((resolve, rejects) => {
        if (url) {
            resolve(fetch(url));
        } else {
            rejects("Url is invalid");
        }
    })
        .then((data) => data.json())
        .catch((err) => console.error(err));
}

//1.
fetchJson(usersJsonUrl)
    .then((res) => console.log(res))
    .catch((err) => console.error(err));
//2.
fetchJson(todosJsonUrl)
    .then((res) => console.log(res))
    .catch((err) => console.error(err));
//3.
fetchJson(usersJsonUrl)
    .then((users) => console.log(users))
    .then(() => fetchJson(todosUrl))
    .then((todos) => console.log(todos))
    .catch((err) => console.error(err));
//4.
fetchJson(todosJsonUrl)
    .then((todos) => {
        let firstUser = todos[0].userId;
        return todos.filter((todo)=>todo.userId==firstUser);
    })
    .then((res) => console.log(res))
    .catch((err) => console.error(err))



